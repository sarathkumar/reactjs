import logo from "./logo.svg";
import React,{useState} from "react";
import "./App.css";

function CounterHook() {
  const [count, setCount] =  useState(0);
  return (
    <div style={{ border: "1px solid black", margin: 12 }}>
      <h1>{count}</h1>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
    </div>
  );
}

class Clock extends React.Component {
  intervelID = 0;

  componentDidMount() {
    this.intervelID = setInterval(() => {
      this.setState({});
    }, 1000);
    console.log(this.intervelID);
  }
  componentWillUnmount() {
    clearInterval(this.intervelID);
  }

  render() {
    return (
      <div>
        <h3>{new Date().toLocaleTimeString()}</h3>
      </div>
    );
  }
}

class Counter extends React.Component {
  state = {
    count: 0,
  };

  countUp = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  countDown = () => {
    this.setState({
      count: this.state.count - 1,
    });
  };

  shouldComponentUpdate() {
    console.log(this.state.count);
    if (this.state.count + 1 == 5) {
      return false;
    }
    return true;
  }

  render() {
    console.log("Counter Render");
    return (
      <div style={{ border: "1px solid black", margin: 12 }}>
        <h1>{this.state.count}</h1>
        <p>{this.props.message}</p>
        <button onClick={this.countUp}>+</button>
        <button onClick={this.countDown}>-</button>
      </div>
    );
  }
}

class App extends React.Component {
  state = {
    clock: false,
    count: 0,
  };
  count = 0;

  clickHandler = () => {
    this.setState({
      clock: !this.state.clock,
    });
  };

  render() {
    console.log("App Render");
    return (
      <div>
        <button onClick={this.clickHandler}>Click</button>
        <h1>Is clock visible : {`${this.state.clock}`}</h1>
        <Counter message={this.state.message}></Counter>
        {this.state.clock && <Clock></Clock>}

        <p>HOOK COUNTER </p>

        <CounterHook></CounterHook>
      </div>
    );
  }
}

export default App;
