import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
//https://raw.githubusercontent.com/tastejs/todomvc-app-css/master/index.css
import './index.css'
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
