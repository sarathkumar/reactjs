function TodoCompose({ onNewTodo }) {
    
  const onSubmit = (event) => {
    event.preventDefault();
    const todoItem = {
      isCompleted: false,
      todo: event.target[0].value,
      id: Date.now(),
    };
    event.target[0].value = "";
    onNewTodo(todoItem);
  };

  return (
    <header className="header">
      <h1>todos </h1>
      <form onSubmit={onSubmit}>
        <input className="new-todo" placeholder="What needs to be done?" />
      </form>
    </header>
  );
}

export default TodoCompose;
