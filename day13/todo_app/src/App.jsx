import { useState } from "react";
import TodoCompose from "./components/compose_todo";
import TodoList from "./components/todo_list";

function TodoApp() {
  const [todos, setTodos] = useState([]);
  const [filter, setFilter] = useState(0);

  return (
    <div className="todoapp">
      <TodoCompose
        onNewTodo={(todo) => {
          setTodos([todo, ...todos]);
        }}
      ></TodoCompose>
      <TodoList
        todos={todos}
        onChange={(todos) => setTodos(todos)}
        filter={filter}
      ></TodoList>
      <footer className="footer">
        <span className="todo-count">
          {todos.filter((todo) => !todo.isCompleted).length} Items left
        </span>

        <ul className="filters">
          <li>
            <a
              href="#"
              className={filter === 0 ? `selected` : ``}
              onClick={() => setFilter(0)}
            >
              All
            </a>
          </li>
          <li>
            <a
              href="#"
              className={filter === 1 ? `selected` : ``}
              onClick={() => setFilter(1)}
            >
              Active
            </a>
          </li>
          <li>
            <a
              href="#"
              className={filter === 2 ? `selected` : ``}
              onClick={() => setFilter(2)}
            >
              Compeleted
            </a>
          </li>
        </ul>
      </footer>
    </div>
  );
}

export default TodoApp;
