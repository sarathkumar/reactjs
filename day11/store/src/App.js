import products from "./products.json";
import "../node_modules/bootstrap/dist/css/bootstrap.css";
import "../node_modules/flickity/dist/flickity.css";
import "./App.css";
import Flickity from "flickity";
import { Component, useState, useRef, useEffect } from "react";

window.Flickity = Flickity;
// function App() {
//   return (
//     <div className="App">
//       <ul>
//         {products.map(({id,image,title,price,description}) => {
//           return (
//             <div key={id} class="card" style={{ margin: 16 }}>
//               <img
//                 className="card-img-top  mx-auto d-block"
//                 src={image}
//                 style={{ width: 200 }}
//                 alt="Card image cap"
//               />
//               <div className="card-img-overlay">
//                 <h5 className="card-title">${price}</h5>
//               </div>
//               <div className="card-body">
//                 <h5 className="card-title">{title}</h5>
//                 <p className="card-text">{description}</p>
//               </div>
//               <ul className="list-group list-group-flush">
//                 <li className="list-group-item">Cras justo odio</li>
//                 <li className="list-group-item">Dapibus ac facilisis in</li>
//                 <li className="list-group-item">Vestibulum at eros</li>
//               </ul>
//               <div className="card-body">
//                 <a href="#" className="card-link">
//                   Buy Now
//                 </a>
//                 <a href="#" className="card-link">
//                   Add To Cart
//                 </a>
//               </div>
//             </div>
//           );
//         })}
//       </ul>
//     </div>
//   );
// }

// export default App;

//HOOKS REFS
// function App() {
//   const userNameRef = useRef();
//   const login = () => {
//     console.log("User name ", userNameRef.current.value);
//   };
//   return (
//     <div style={{ margin: 16 }}>
//       <input ref={userNameRef}></input>
//       <p>Hello</p>
//       <button onClick={login}>Login </button>
//     </div>
//   );
// }
//CLASS REFS
// class App extends Component {
//   username = "";
//   constructor(props) {
//     super(props);
//     this.userNameRef = createRef();
//   }
//   login = () => {
//     console.log(this.username);
//   };

//   render() {
//     return (
//       <div style={{ margin: 16 }}>
//         <input
//           ref={this.userNameRef}
//           onChange={(e) => (this.username = e.target.value)}
//         ></input>
//         <p>Hello</p>
//         <button onClick={this.login}>Login </button>
//       </div>
//     );
//   }
// }

//USING EXTERNAL VANILLA LIBS

function App() {
  
  const [show, setShow] = useState(false);

  return (
    <div>
      <button onClick={() => setShow(!show)}>SHOW</button>
      {show && <Carousel></Carousel>}
    </div>
  );
}

function Carousel() {
  const carouselRef = useRef();

  useEffect(() => {
    if (carouselRef.current != null)
      var flkty = new Flickity(carouselRef.current, {
        // options
        cellAlign: "left",
        contain: true,
      });
    return () => {
      flkty.remove();
    };
  }, []);

  return (
    <>
      <div className="main-carousel" ref={carouselRef}>
        <div className="carousel-cell">...</div>
        <div className="carousel-cell">...</div>
        <div className="carousel-cell">...</div>
      </div>
    </>
  );
}
export default App;
