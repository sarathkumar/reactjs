import { useState } from "react";
import { useEffect, Component } from "react";
import { Link, Outlet, useParams } from "react-router-dom";
import "./expense.css";
const expenses = [
  {
    title: "Ad",
    price: 100,
    id: "ad-expense",
  },
  {
    title: "PR",
    price: 500,
    id: "exp-PR",
  },
];
export default function Expenses() {
  return (
    <div>
      <h1>Expenses</h1>
      <div className="container">
        <div className="sidebar">
          <ul>
            {expenses.map((item) => {
              return (
                <li>
                  <Link to={`/expenses/${item.id}`}>{item.title}</Link>
                </li>
              );
            })}
          </ul>
        </div>
        <div className="mainContent">
          <Outlet></Outlet>
        </div>
      </div>
    </div>
  );
}

function _ExpenseDetail() {
  const param = useParams();
  const [show, setShow] = useState(false);
  const expense = expenses.filter((ex) => ex.id === param.id)[0];
  const hello = null;
  useEffect(() => {
    document.title = `Expense : ${expense.title}`;
  }, [expense]);
  return (
    <div>
      <h4>Expense : {expense.title}</h4>
      <h4>Price : ${expense.price}</h4>

      {show && <p>{hello.text}</p>}

      <button onClick={() => setShow(true)}>💣</button>
    </div>
  );
}
export const ExpenseDetail = () => (
  <ErrorBoundary>
    <_ExpenseDetail></_ExpenseDetail>
  </ErrorBoundary>
);

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {}

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }

    return this.props.children;
  }
}
