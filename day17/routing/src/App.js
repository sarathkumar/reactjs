import logo from "./logo.svg";
import "./App.css";
import { Link, Outlet } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <Link className="App-link" to={"/expenses"}>
          Expenses
        </Link>
        <Link className="App-link" to={"/about"}>
          About
        </Link>
        <Link className="App-link" to={"/invoices"}>
          invoices
        </Link>
      </header>
      <Outlet />
    </div>
  );
}

export default App;
