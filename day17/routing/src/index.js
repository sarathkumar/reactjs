import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Expenses, { ExpenseDetail } from "./expense";
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";
import reportWebVitals from "./reportWebVitals";

const About = () => (
  <div>
    <h1>About</h1>
    <Outlet></Outlet>
  </div>
);
const Invoices = () => <h1>Invoices</h1>;
const Contact = () => <h1>Contact</h1>;

ReactDOM.render(
  <div className="App">
    <React.StrictMode>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App />}>
            <Route path="invoices" element={<Invoices />} />
          </Route>
          <Route path="expenses" element={<Expenses />}>
            <Route path=":id" element={<ExpenseDetail />} />
          </Route>
          <Route path="about" element={<About />}>
            <Route path="contact" element={<Contact />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </React.StrictMode>
  </div>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
