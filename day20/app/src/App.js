import logo from "./logo.svg";
import "./App.css";
import styled, { css } from "styled-components";

const Button = styled.h2`
  /* This renders the buttons above... Edit me! */
  display: inline-block;
  border-radius: 3px;
  padding: 0.5rem 0;
  margin: 0.5rem 1rem;
  width: 11rem;
  background: transparent;
  color: white;
  border: 2px solid white;
  ${(props) => {
    if (props.borderSize && props.borderColor) {
      return css`
        border: ${props.borderSize}px solid ${props.borderColor};
      `;
    }
  }}
  ${(props) =>
    props.primary &&
    css`
      background: white;
      color: black;
    `}
`;

// function Button({ children, color = "red" }) {
//   return (
//     <div
//       style={{
//         width: 90,
//         height: 34,
//         alignItems: "center",
//         background: color,
//         cursor: "pointer",
//       }}
//     >
//       {children}
//     </div>
//   );
// }
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Button primary>
          <span>Click me</span>
        </Button>
        <Button color="green">
          <span>Click me</span>
        </Button>
        <Button  borderColor="red" borderSize={8}>
          <span>With border</span>
        </Button>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
