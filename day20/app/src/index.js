import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import DomServer from 'react-dom/server'
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
); 
console.log(DomServer.renderToString(<App />))