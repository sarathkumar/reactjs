const URL = `https://fakestoreapi.com/products/`;
import { useEffect, useState } from "react";
import styles from "../../styles/Product.module.css";
import useSWR from "swr";
import Img from "next/image";

const fetcher = (url) => fetch(url).then((r) => r.json());

export default function Products() {
  //   const [products, setProducts] = useState([]);
  let products = [];
  let { data, error } = useSWR(URL, fetcher);
  if (data) {
    products = data;
  }
  //   useEffect(() => {
  //     fetch(URL)
  //       .then((res) => res.json())
  //       .then((response) => {
  //         setProducts(response);
  //       });
  //   });

  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <div className={styles.grid}>
          {products.map((item) => {
            return (
              <div className={styles.card} key={item.id}>
                <h4>{item.title}</h4>
                <p>${item.price}</p>
                <Img
                  alt="Product"
                  width={500}
                  src={item.image}
                  height={500}
                ></Img>
              </div>
            );
          })}
        </div>
      </main>
    </div>
  );
}
