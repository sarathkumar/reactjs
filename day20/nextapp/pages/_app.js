import '../styles/globals.css'

function MyApp({ Component, pageProps }) {
  return <div id='MyROot'>
    <Component {...pageProps} />
  </div>
}

export default MyApp
