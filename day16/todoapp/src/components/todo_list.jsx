import { useDispatch, useSelector, useStore } from "react-redux";
import { toggleComplete } from "../state/todo";

function TodoList() {
  console.log("TODO LIST RENDERING")
  const todos = useSelector((state) => state.todo.todos);
  const filter = useSelector((state) => state.todo.filter);
  const dispatch = useDispatch();

  function getFilteredTodos() {
    if (filter === 0) {
      return todos;
    }
    if (filter === 2) {
      return todos.filter((todo) => todo.isCompleted);
    }
    if (filter === 1) {
      return todos.filter((todo) => !todo.isCompleted);
    }
  }

  const toggleTodo = (id) => {
    dispatch(toggleComplete(id));
  };

  const deleteTodo = (id) => {
    const todoItems = todos.filter((todo) => todo.id != id);
  };

  return (
    <section className="main">
      <ul className="todo-list">
        {getFilteredTodos().map((todo) => {
          return (
            <li className={todo.isCompleted ? `completed` : ""} key={todo.id}>
              <div className="view">
                <input
                  className="toggle"
                  value={todo.isCompleted}
                  type="checkbox"
                  checked={todo.isCompleted}
                  onChange={() => toggleTodo(todo.id)}
                />
                <label>
                  {todo.todo} {todo.isCompleted ?? "TRUE"}
                </label>
                <button
                  className="destroy"
                  onClick={() => deleteTodo(todo.id)}
                ></button>
              </div>
              <input className="edit" />
            </li>
          );
        })}
      </ul>
    </section>
  );
}

export default TodoList;
