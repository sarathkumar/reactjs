import { setFilter } from "../state/todo";
import { useDispatch, useSelector } from "react-redux";

export default function Filters() {
  const filter = useSelector((state) => state.todo.filter);
  const dispatch = useDispatch();

  return (
    <ul className="filters">
      <li>
        <a
          href="#"
          className={filter === 0 ? `selected` : ``}
          onClick={() => dispatch(setFilter(0)) }
        >
          All
        </a>
      </li>
      <li>
        <a
          href="#"
          className={filter === 1 ? `selected` : ``}
          onClick={() => dispatch(setFilter(1))}
        >
          Active
        </a>
      </li>
      <li>
        <a
          href="#"
          className={filter === 2 ? `selected` : ``}
          onClick={() =>dispatch(setFilter(2))}
        >
          Compeleted
        </a>
      </li>
    </ul>
  );
}
