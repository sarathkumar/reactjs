import { addTodo, compose } from "../state/todo";
import { useDispatch, useSelector } from "react-redux";

function TodoCompose() {

  const dispatch = useDispatch();
  const composeValue = useSelector((state) => state.todo.composeText);

  const onSubmit = (event) => {
    event.preventDefault();
    const todoItem = {
      isCompleted: false,
      todo: event.target[0].value,
      id: Date.now(),
    };
    event.target[0].value = "";
    const action = addTodo(todoItem);
    dispatch(action);
  };

  return (
    <header className="header">
      <h1>todos </h1>
      <form onSubmit={onSubmit}>
        <input
          value={composeValue}
          className="new-todo"
          onChange={(event) => dispatch(compose(event.target.value))}
          placeholder="What needs to be done?"
        />
      </form>
    </header>
  );
}

export default TodoCompose;
 

///Prop Drilling -> HOC/Context -> useReducer -> Redux/MobX/Flux



