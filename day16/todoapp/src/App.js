import { useState } from "react";
import TodoCompose from "./components/compose_todo";
import TodoList from "./components/todo_list";
import Filters from "./components/filter_todo";
import { useSelector } from "react-redux";

function App() {
  return (
    <div className="todoapp">
      <TodoCompose></TodoCompose>
      <TodoList></TodoList>
      <footer className="footer">
        <Count></Count>
        <Filters></Filters>
      </footer>
    </div>
  );
}
function Count() {
  console.log("COUNT RENDERING");
  const todos = useSelector((state) => state.todo.todos);
  return (
    <span className="todo-count">
      {todos.filter((todo) => !todo.isCompleted).length} Items left
    </span>
  );
}

export default App;
