import { createSlice } from "@reduxjs/toolkit";

export const todoSlice = createSlice({
  name: "Todo",
  initialState: {
    todos: [],
    composeText: "",
    filter: 0,
  },
  reducers: {
    addTodo(state, action) {
      state.todos.push(action.payload);
    },
    compose(state, action) {
      state.composeText = action.payload;
    },
    setFilter(state, action) {
      state.filter = action.payload;
    },
    toggleComplete(state, { payload }) {
      state.todos = state.todos.map((todo) => {
        if (todo.id === payload) {
          todo.isCompleted = !todo.isCompleted;
        }
        return todo;
      });
    },
  },
});
export const { addTodo, setFilter, toggleComplete, compose } =
  todoSlice.actions;
export default todoSlice.reducer;
