const btn = document.getElementById("btn");

//ARROW FUNC
const callBack = () => {
  //   console.log("called", this);
};

btn.addEventListener("click", function () {
  callBack();
});

//TEMPLATE LITERALS
let a = 10;
// const b = "Value of a = " + a;
const b = `Value of a = ${a == 10 ? "Ten" : "somethingelse"}`;

//CLASS
class Vehicle {
  tyres = 4;
  brand = "";

  constructor(brandName) {
    this.brand = brandName;
  }

  print() {
    console.log(`Vehicle Brand ${this.brand}`);
  }
}

const vehicle = new Vehicle("Tata");
// vehicle.print();

//DEFAULT PARAMS
const colorPrinter = (message, color = "#5EB0D7") => {
  console.log(`%c ${message} `, `background: #222; color: ${color}`);
};

//SPREAD OPERATOR
const ab = [1, 2, 3, 4];
const abc = [24, 25, 26];

// console.log([...ab, ...abc]);

const cd = { name: "Abc", age: 10 };
const cde = { dob: "10-10-1111", notes: "" };

// console.log({ ...cde, age: 22, ...cd });

//Object destructuring
btn.addEventListener("click", ({ target, shiftKey }) => {
  if (shiftKey) {
    target.textContent = "Clicked";
  }
});

const Student = {
  name: "Adith",
  age: 6,
};

let { age, name } = Student;

console.log(age, name);

//Array destructuring
const xy = [1, 2];
let [aaa, bbb, ccc=10] = xy;
console.log(aaa, bbb, ccc);


