import React from "react";
import "./App.css";

// function Box({ size = "", color = "red", children = [], ...rest }) {
//   return (
//     <div className={`box-${size} ${color}`} {...rest}>
//       <div className="box-content">{children}</div>
//     </div>
//   );
// }

// function App() {
//   return (
//     <div className="App">
//       <Box color="red" size="large"></Box>
//       <Box color="yellow" size="medium"></Box>
//       <Box style={{ color: "red", backgroundColor: "grey" }} id="hello">
//         <p>Hello</p>
//       </Box>
//     </div>
//   );
// }
// export default App;

function onSubmit(event) {
  event.preventDefault();
}

// function App() {
//   return (
//     <div className="App">
//       <form >
//         <input onChange={} type="text"></input>
//         <button>Click me</button>
//       </form>
//       <p>Hell {}</p>
//     </div>
//   );
// }

class App extends React.Component {
  state = {
    message: "hello",
    count: 0,
  };

  click = (event) => {
    this.setState({
      message: "Clicked",
      count: this.state.count + 1,
    });
  };

  onChange = (event) => {
    this.setState({
      // message: event.target.value,
    });
  };

  render() {
    return (
      <div className="App">
        <input
          type="text"
          onChange={this.onChange}
          value={this.state.message}
        ></input>
        <button onClick={this.click}>Click me</button>
        <p>Hello {this.state.message}</p>
        <p>Count {this.state.count}</p>
      </div>
    );
  }
}

export default App;
