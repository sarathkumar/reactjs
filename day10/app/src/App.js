import logo from "./logo.svg";
import "./App.css";
import React, { useState, useEffect } from "react";

// function Counter() {
//   const [value, setValue] = useState(0);
//   const [show, setShow] = useState(true);

//   useEffect(() => {
//     const counter = setInterval(() => {
//       setValue(value + 1);
//     }, 1000);
//     return () => {
//       clearInterval(counter);
//     };
//   }, [value]);

//   return (
//     <div className="App">
//       <p>{value}</p>
//       <button onClick={() => setValue(value + 1)}>UP</button>
//       {show && <p>This Is a Message</p>}
//       <button onClick={() => setShow(!show)}>
//         {show ? "Hide Message" : "Show Message"}
//       </button>
//     </div>
//   );
// }

// function App() {
//   const [show, setShow] = useState(true);

//   return (
//     <div>
//       <button onClick={() => setShow(!show)}>{show ? "Hide" : "Show"}</button>
//       {show && <Counter />}
//     </div>
//   );
// }

// export default App;

//LIST RENDERING
// function shuffle(array) {
//   let currentIndex = array.length,
//     randomIndex;

//   // While there remain elements to shuffle...
//   while (currentIndex != 0) {
//     // Pick a remaining element...
//     randomIndex = Math.floor(Math.random() * currentIndex);
//     currentIndex--;

//     // And swap it with the current element.
//     [array[currentIndex], array[randomIndex]] = [
//       array[randomIndex],
//       array[currentIndex],
//     ];
//   }

//   return [...array];
// }

// const list = [
//   { item: "Mango", id: 1 },
//   { item: "Apple", id: 2 },
//   { item: "Grapes", id: 3 },
//   { item: "Kiwi", id: 4 },
// ];

// function App() {
//   const [fruits, setFruit] = useState(list);

  useEffect(() => {
    const id = setInterval(() => {
      setFruit(shuffle(fruits));
    }, 2000);
    return () => clearInterval(id);
  }, [fruits]);

//   const add = () => {
//     setFruit(shuffle(fruits));
//   };

//   return (
//     <div>
//       <ol>
//         {fruits.map((fruit) => {
//           return (
//             <li key={fruit.id} id={fruit.id}>
//               {fruit.item}
//               <input defaultValue={fruit.item}></input>
//             </li>
//           );
//         })}
//       </ol>
//       <button onClick={add}>ADD</button>
//     </div>
//   );
// }

//React Fragments
// function App() {
//   return [<p key="A">Hello</p>, <p key="VB">Hello</p>];
// }

//Controlled and uncontrolled

function App() {

  const [name,setName] = useState("")

  return (
    <div>
      <p>Name Is {name.toLowerCase()}</p>
      <input value={name.toLowerCase()} onChange={(event)=>setName(event.target.value)} ></input>
    </div>
  );
}

export default App;
