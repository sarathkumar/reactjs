// Higher order Function

function fiveProvider(param) {
  param(5);
}

function abc() {
  return function () {
    return function () {
      return abc;
    };
  };
}

// fiveProvider(function (value) {
//   console.log(`Value is ${value}`);
// });

// Pure Function

const obj = {
  x: 1,
};

const add = (a, b) => a + b;

// LINK@{https://fakestoreapi.com/products/}
const json = [];
json
.filter(product=>product.category === "men's clothing")
.map(product=>product.price)
.reduce((prev,current)=>prev+current)
