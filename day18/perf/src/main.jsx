import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import ExampleOne from './components/example.1'
import GlobeComponent from './components/globe'
import { BrowserRouter, Routes, Route, Outlet } from "react-router-dom";

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}></Route>
        <Route path="example1" element={<ExampleOne />}></Route>
        <Route path="globe" element={<GlobeComponent />}></Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById("root")
);
