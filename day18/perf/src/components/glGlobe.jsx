import Globe from 'react-globe.gl';

export default function GLGlobe({image}) {
  const N = 300;
  const gData = [...Array(N).keys()].map(() => ({
    lat: (Math.random() - 0.5) * 180,
    lng: (Math.random() - 0.5) * 360,
    size: Math.random() / 3,
    color: ["red", "white", "blue", "green"][Math.round(Math.random() * 3)],
  }));

  return (
    <Globe
      globeImageUrl={image}
      pointsData={gData}
      pointAltitude="size"
      pointColor="color"
    />
  );
}
