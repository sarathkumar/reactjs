import { useState, memo } from "react";
import Countries from "./countries.json";

export default function ExampleOne() {
  const [state, setState] = useState([]);
  const [selected, setSelected] = useState([]);
  return (
    <div style={{ display: "flex" }}>
      <div style={{ width: "25vw", padding: 16, background: "#dedede" }}>
        {Object.keys(Countries).map((item) => {
          return (
            <Country
              key={item}
              country={item}
              selected={item === selected}
              onClick={() => {
                setState(Countries[item]);
                setSelected(item);
              }}
            ></Country>
          );
        })}
      </div>
      <div style={{ width: "75vw", padding: 16, background: "#b4b4b4" }}>
          <StateList state={state}/>
      </div>
    </div>
  );
}
function Country({ onClick, country, selected }) {
  return (
    <p
      style={{
        background: selected ? "#5fafee" : null,
        margin: 0,
        padding: 12,
      }}
      onClick={onClick}
    >
      {country}
    </p>
  );
}
Country = memo(Country);

function StateList({state}) {
  return (
    <ol>
      {state.map((item) => {
        return <li key={item}>{item}</li>;
      })}
    </ol>
  );
}
