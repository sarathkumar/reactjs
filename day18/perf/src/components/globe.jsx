import { useState, lazy, Suspense } from "react";
const GLGlobe = lazy(() => import("./glGlobe"));
import image from "../assets/earth-night.jpeg";

export default function GlobeComponent() {
  // simple and extensive options to confi  gure globe
  return (
    <div style={{ margin: 14 }}>
      <p>Hello</p>
      <Suspense fallback={<p>Loading...</p>}>
        <GLGlobe image={image}></GLGlobe>
      </Suspense>
    </div>
  );
}
