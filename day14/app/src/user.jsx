import wrapWithUser from "./user_provider";

function UserComponent({ name = "", age = "", dob = "", setUser, ...rest }) {
  return (
    <div {...rest}>
      <p>Name : {name}</p>
      <p>Age : {age}</p>
      <p>Dob : {dob}</p>
      <button
        onClick={() => {
          setUser({ name: "Sarath", age: 48, dob: "" });
        }}
      >
        Set User
      </button>
    </div>
  );
}

export default wrapWithUser(UserComponent);
