import "./App.css";
import UserComponent from "./user";
import {
  createContext,
  Component,
  useState,
  useContext,
  useEffect,
} from "react";

//BORDER COLOR EXAMPLE

// function MessageBox({ name = "John" }) {
//   return (
//     <div>
//       <p>Greeting {name}</p>
//     </div>
//   );
// }
// function RedBox() {
//   return <div style={{ width: 200, height: 200 }}></div>;
// }

// const BorderMessageBox = wrapWithBorder(MessageBox);
// const RedBoxWithBorder = wrapWithBorder(RedBox, "green");

// function App() {
//   return (
//     <div className="App" style={{ margin: 15 }}>
//       <h1>Hello</h1>
//       <MessageBox></MessageBox>
//       <BorderMessageBox ></BorderMessageBox>
//       <RedBoxWithBorder />
//     </div>
//   );
// }

// function wrapWithBorder(Component, color = "red") {
//   return function () {
//     return (
//       <div style={{ border: `5px solid ${color}` }}>
//         <Component name="Someone"></Component>
//       </div>
//     );
//   };
// }

// export default App;

// function App() {
//   return (
//     <div className="App" style={{ margin: 15 }}>
//         <UserComponent style={{backgroundColor:"red"}}></UserComponent>
//     </div>
//   );
// }

const TodosContext = createContext([]);
const SetTodoContext = createContext(() => {});

function ComposeTodo() {
  const setTodo = useContext(SetTodoContext);
  const todos = useContext(TodosContext);
  return (
    <div>
      <button
        onClick={() => {
          setTodo([
            ...todos,
            {
              id: Date.now(),
              todo: `Todo ${Date().toString()}`,
            },
          ]);
        }}
      >
        Add
      </button>
    </div>
  );
}

function ListTodo() {
  const todos = useContext(TodosContext);
  return (
    <ul>
      {todos.map((item) => (
        <li key={item.id}>{item.todo}</li>
      ))}
    </ul>
  );
}

class CountTodo extends Component {
  render() {
    return (
      <div>
        <TodosContext.Consumer>
          {(value) => <p>Count: {value.length}</p>}
        </TodosContext.Consumer>
      </div>
    );
  }
}

function ListPage() {
  return (
    <div>
      <ListTodo></ListTodo>
    </div>
  );
}

function App() {
  return (
    <div>
      <TodoStateProvider>
        <ComposeTodo></ComposeTodo>
        <CountTodo></CountTodo>
        <ListPage></ListPage>
      </TodoStateProvider>
    </div>
  );
}

function TodoStateProvider({ children }) {
  const [todo, setTodo] = useState([]);
  return (
    <TodosContext.Provider value={todo}>
      <SetTodoContext.Provider value={setTodo}>
        {children}
      </SetTodoContext.Provider>
    </TodosContext.Provider>
  );
}
export default App;
