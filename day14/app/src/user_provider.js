import { useState } from "react";

function wrapWithUser(Component) {
  return function (props) {
    const [user, setUser] = useState({ name: "", age: "", dob: "" });
    return <Component {...user} setUser={setUser} {...props}></Component>;
  };
}
export default wrapWithUser;
