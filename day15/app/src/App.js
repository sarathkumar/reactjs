import logo from "./logo.svg";
import "./App.css";
import {
  useEffect,
  useContext,
  useState,
  useReducer,
  createContext,
  createRef,
} from "react";

// function fibonacci(n) {
//   console.log("Calculating...");
//   return n < 1 ? 0 : n <= 2 ? 1 : fibonacci(n - 1) + fibonacci(n - 2);
// }

// function App() {
//   const [number, setNumber] = useState(1);
//   const [date, setDate] = useState("");
//   useEffect(() => {
//     const id = setInterval(() => {
//       setDate(Date().toString());
//     }, 1000);
//     return () => {
//       clearInterval(id);
//     };
//   }, []);
//   const fib = useMemo(() => fibonacci(number), [number]);

//   return (
//     <div className="App">
//       <header className="App-header">
//         <h1>{number}</h1>
//         <h5>FIB : {fib}</h5>
//         <button onClick={() => setNumber(number + 1)}>Add Numer</button>
//         <h6>{date}</h6>
//       </header>
//     </div>
//   );
// }

const reducer = (state, { action, payload }) => {
  const { count } = state;
  console.log(action);
  if (action == "UP") {
    return {
      ...state,
      count: count + (payload ?? 1),
    };
  }
  if (action == ) {
    return {
      ...state,
      count: count - (payload ?? 1),
    };
  }
  if (action == "LOGIN") {
    return {
      ...state,
      name: payload.name,
      password: payload.password,
    };
  }
  return state;
};

const initState = {
  count: 0,
  name: "",
  password: "",
};

const StoreContext = createContext(initState);
const DispatchContext = createContext(() => {});

const history = [];
window.AppHistory = history;

function useReducerWithLogging(reducer, initState) {
  const [state, dispatch] = useReducer(reducer, initState);
  history.push(state);
  return [
    state,
    (action) => {
      dispatch(action);
      history.push(action);
    },
  ];
}

function Store({ children }) {
  const [state, dispatch] = useReducerWithLogging(reducer, initState);
  return (
    <StoreContext.Provider value={state}>
      <DispatchContext.Provider value={dispatch}>
        {children}
      </DispatchContext.Provider>
    </StoreContext.Provider>
  );
}

function App() {
  return (
    <div>
      <Store>
        <div className="App">
          <header className="App-header">
            <Counter></Counter>
            <div style={{ margin: 15 }}></div>
            <Login></Login>
          </header>
        </div>
      </Store>
    </div>
  );
}

function Login() {
  const dispatch = useContext(DispatchContext);
  const state = useContext(StoreContext);
  const nameRef = createRef();
  const passwordRef = createRef();
  const submit = () => {
    dispatch({
      action: "LOGIN",
      payload: {
        name: nameRef.current.value,
        password: passwordRef.current.value,
      },
    });
  };
  return (
    <div>
      <input ref={nameRef} placeholder="Name"></input>
      <input ref={passwordRef} type="password" placeholder="Password"></input>
      <button onClick={submit}>Submit</button>
      <pre>{JSON.stringify(state)}</pre>
    </div>
  );
}

function Counter() {
  const state = useContext(StoreContext);
  const dispatch = useContext(DispatchContext);
  const { count } = state;
  return (
    <div>
      <p>Counter {count}</p>
      <button
        onClick={() =>
          dispatch({
            action: "UP",
          })
        }
      >
        +
      </button>
      <button
        onClick={() =>
          dispatch({
            action: "DOWN",
          })
        }
      >
        -
      </button>
      <button
        onClick={() =>
          dispatch({
            action: "UP",
            payload: 5,
          })
        }
      >
        Add 5
      </button>
    </div>
  );
}

export default App;
