import { SatelliteRounded } from "@mui/icons-material";
import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: {
    email: "",
    name: "",
    uid: "",
  },
  chatUsers: [],
};
export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUser: (state, { payload }) => {
      state.user = payload;
    },
    setChatUsers: (state, { payload }) => {
      state.chatUsers = payload;
    },
  },
});

export const { setUser, setChatUsers } = userSlice.actions;
export default userSlice.reducer;
