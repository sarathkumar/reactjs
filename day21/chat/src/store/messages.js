import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  messages: [],
};
export const messageSlice = createSlice({
  name: "message",
  initialState,
  reducers: {
    addMessage: (state, { payload }) => {
      state.messages = payload;
    },
    clearMessage: (state, { payload }) => {
      state.messages = [];
    },
  },
});

export const { addMessage, clearMessage } = messageSlice.actions;
export default messageSlice.reducer;
