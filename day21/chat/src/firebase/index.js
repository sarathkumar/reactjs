// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getDatabase } from "firebase/database";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyDOsn_KfXyi5pNUcO2lKdjMU6crI795u50",
  authDomain: "chatapp-80371.firebaseapp.com",
  databaseURL:
    "https://chatapp-80371-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "chatapp-80371",
  storageBucket: "chatapp-80371.appspot.com",
  messagingSenderId: "449145049815",
  appId: "1:449145049815:web:3cac350119c713b33c1692",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const database = getDatabase();

export { app, database };
