import {
  Container,
  Link,
  List,
  ListItem,
  ListItemText,
  Button,
  Chip,
  TextField,
  Avatar,
  Stack,
  ListItemAvatar,
  Typography,
  Grid,
  Paper,
  Box,
  Divider,
  Skeleton,
} from "@mui/material";
import { Outlet, useParams } from "react-router-dom";
import ImageIcon from "@mui/icons-material/Person";
import ChatMessageList from "./chat_message_list";
import ChatCompose from "./chat_compose";
import { useEffect, useState } from "react";
import { findUser } from "../../firebase/database";

function ChatUser() {
  const { chatid } = useParams();
  const [chatUser, setChatUser] = useState({ name: "", email: "" });

  useEffect(() => {
    console.log(chatid);
    findUser(chatid).then((user) => setChatUser(user));
  }, [chatid]);

  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar>
          <ImageIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={chatUser.name}
        secondary={chatUser.email}
      ></ListItemText>
    </ListItem>
  );
}

export default function ChatDetail() {
  const param = useParams();
  return (
    <Grid
      container
      direction="column"
      justifyContent="space-evenly"
      alignItems="flex-start"
      sx={{
        width: "100%",
        height: "100%",
      }}
    >
      <Grid item style={{ width: "100%", height: "30vh" }} sm={10}>
        <ChatUser />
        <Divider variant="middle" />
        <ChatMessageList></ChatMessageList>
      </Grid>
      <Grid
        item
        style={{ width: "100%", height: "100%" }}
        alignContent="center"
        sm={1}
      >
        {" "}
        <ChatCompose />
      </Grid>
    </Grid>
  );
}
