import { Button, Grid, Box } from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import FormControl, { useFormControl } from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import LayoutBase from "./layout_base";
import ChatDashboard from "./chat_dashboard";
import SingUp from "./sign_up";
import SignIn from "./sign_in";
import { useEffect } from "react";
import { Outlet } from "react-router-dom";
import { getAuth, onAuthStateChanged } from "firebase/auth";
import { useNavigate } from "react-router-dom";
import { findUser, subscribeToUsers } from "../firebase/database";
import { setUser } from "../store/user";
import { useDispatch } from "react-redux";

export default function Landing() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    const auth = getAuth();
    console.log(Location);
    onAuthStateChanged(auth, async (loggedInUser) => {
      if (loggedInUser) {
        console.log("Logged in ", loggedInUser);
        const user = await findUser(loggedInUser.uid);
        dispatch(setUser(user));
        subscribeToUsers(dispatch, loggedInUser.uid);
        navigate("/chat");
      } else {
        console.log("Logged out");
        navigate("/signin");
      }
    });
  }, []);

  return (
    <LayoutBase>
      <Outlet></Outlet>
    </LayoutBase>
  );
}
