import {
  Container,
  List,
  ListItem,
  ListItemText,
  Avatar,
  Stack,
  ListItemAvatar,
  Typography,
  Grid,
  Paper,
  Box,
  Divider,
} from "@mui/material";
import ImageIcon from "@mui/icons-material/Person";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function UserDetails() {
  const user = useSelector((state) => state.user.user);
  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar sx={{ width: 56, height: 56, bgcolor: "cornflowerblue" }}>
          <ImageIcon />
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        style={{ paddingLeft: 8 }}
        primary={user.name}
        secondary={user.email}
      ></ListItemText>
    </ListItem>
  );
}
export default function ChatSideBar() {
  return (
    <Box sx={{ width: "100%", maxWidth: 360 }}>
      <Box sx={{ my: 3, mx: 2 }}>
        <Grid container alignItems="center">
          <UserDetails></UserDetails>
        </Grid>
        <Typography color="text.secondary" variant="body2"></Typography>
      </Box>
      <Divider variant="middle" />

      <Box sx={{ m: 2 }}>
        <ChatUsersList></ChatUsersList>
      </Box>
    </Box>
  );
}

function ChatUsersList() {
  const chatUsers = useSelector((state) => state.user.chatUsers);

  return (
    <List
      sx={{
        width: 340,
        maxHeight: "60%",
        overflow: "auto",
        scrollbarWidth: 0,
      }}
    >
      {chatUsers.map((item) => {
        return (
          <Link to={`/chat/${item.uid}`}>
            <ListItem>
              <ListItemAvatar>
                <Avatar>
                  <ImageIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={item.name}
                secondary={item.email}
              ></ListItemText>
            </ListItem>
          </Link>
        );
      })}
    </List>
  );
}
