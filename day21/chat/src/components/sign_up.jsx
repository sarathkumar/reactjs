import {
  Container,
  Link,
  Button,
  TextField,
  Grid,
  Paper,
  Box,
} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import FormControl, { useFormControl } from "@mui/material/FormControl";
import OutlinedInput from "@mui/material/OutlinedInput";
import LayoutBase from "./layout_base";
import { useNavigate } from "react-router-dom";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { useState } from "react";
import { getDatabase, set, ref, push } from "firebase/database";

export default function SingUp() {
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();

  const handleSubmit = (event) => {
    event.preventDefault();
    const auth = getAuth();
    const database = getDatabase();
    const name = event.target[0].value;
    const email = event.target[2].value;
    const password = event.target[4].value;
    setLoading(true);
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        setLoading(false);
        const user = userCredential.user;
        const dbUser = {
          name,
          email,
          uid: user.uid,
        };
        const userRef = ref(database, "users");
        const pushRef = push(userRef);
        set(pushRef, dbUser);
        navigate("/signin")
      })
      .catch((error) => {
        setLoading(false);
        const errorCode = error.code;
        const errorMessage = error.message;
        // ..
      });
  };

  return (
      <Grid
        container
        align="center"
        justify="center"
        direction="row"
        component="main"
        sx={{ height: "94vh" }}
      >
        <Grid item sm={4} md={4}>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              padding: 6,
            }}
          >
            <Box
              component="form"
              onSubmit={handleSubmit}
              noValidate
              sx={{ mt: 24 }}
            >
              <h2>Create an account</h2>
              <FormControl sx={{ mt: 0 }} required fullWidth>
                <OutlinedInput placeholder="Name" />
              </FormControl>
              <FormControl sx={{ mt: 2 }} required fullWidth>
                <OutlinedInput placeholder="Email" type="email" />
              </FormControl>
              <FormControl sx={{ mt: 2 }} required fullWidth>
                <OutlinedInput placeholder="Password" type="password" />
              </FormControl>
              <LoadingButton
                type="submit"
                fullWidth
                loading={loading}
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Sign Up
              </LoadingButton>
              <Grid>
                <Grid>
                  <p>Already have an account?</p>
                </Grid>
                <Grid>
                  <Button
                    onClick={() => {
                      navigate("/signin");
                    }}
                  >
                    Login
                  </Button>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
        <Grid
          item
          xs={false}
          sm={8}
          md={8}
          sx={{
            backgroundImage:
              "url(https://source.unsplash.com/random/?city,night,nature)",
            backgroundRepeat: "no-repeat",
            width: "100%",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
      </Grid>
  );
}
