import { useEffect } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ChatDashboard from "./components/chat_dashboard";
import ChatDetail from "./components/chat_details";
import Landing from "./components/landing";
import SignIn from "./components/sign_in";
import SignUp from "./components/sign_up";
 
function App() {
 
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Landing />}>
          <Route path="/signin" element={<SignIn />}></Route>
        <Route path="/signup" element={<SignUp />}></Route>
        <Route path="/chat" element={<ChatDashboard />}>
          <Route path=":chatid" element={<ChatDetail></ChatDetail>}></Route>
        </Route>
        </Route>
        
      </Routes>
    </BrowserRouter>
  );
}

export default App;
