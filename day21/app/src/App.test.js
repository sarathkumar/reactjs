import { render, screen, fireEvent } from "@testing-library/react";
import App from "./App";
import ReactDOM from "react-dom";

// test("Counter Test", () => {
//   const div = document.createElement('div')
//   ReactDOM.render(<App></App>,div)

//   const buttons  = div.querySelectorAll("button");
//   const [plusButton,negativeButton] = buttons

//   const h3 = div.querySelector("h3");

//   plusButton.click();
//   expect(h3.textContent).toBe("Counter 1")
//   negativeButton.click();
//   expect(h3.textContent).toBe("Counter 0")
//   negativeButton.click();
//   expect(h3.textContent).not.toBe("Counter 0")

// });

test("Counter test", async () => {
  render(<App></App>);
  const element = screen.getByRole("heading");
  expect(element.textContent).toBe("Counter 0");
});

test("Counter + test", async () => {
  render(<App></App>);
  const element = screen.getByRole("heading");
  const counterElement =screen.getByText(/enter 0/i)
  const plus = screen.getByRole("button", {
    name: /\+/i,
  });
 

  fireEvent.click(plus);
  fireEvent.mouseOver(plus)
  expect(element.textContent).toBe("Counter 1");
  expect(counterElement.textContent).toBe("Enter 1");
});

test("Counter - test", async () => {
  render(<App></App>);
  const element = screen.getByRole("heading");
  const minus = screen.getByRole("button", {
    name: /\-/i,
  });
  fireEvent.click(minus);
  expect(element.textContent).toBe("Counter -1");
});
