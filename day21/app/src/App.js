import logo from './logo.svg';
import './App.css';
import {useState} from 'react'

function App() {
  const [count,setCount ] = useState(0);
  const [onEnter,setEnter ] = useState(0);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
         <h3>Counter {count}</h3>
         <span>Enter {onEnter}</span>
         <button onMouseEnter={()=>setEnter(onEnter+1)} onClick={()=>setCount(count+1)}>+</button>
         <button onClick={()=>setCount(count-1)}>-</button>
      </header>
    </div>
  );
}

export default App;
