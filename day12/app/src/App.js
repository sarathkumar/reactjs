import logo from "./logo.svg";
import "./App.css";
import { useEffect, useState } from "react";
import { useSpring, animated, to } from "react-spring";

function useStorage(defaultValue, key) {
  const [value, setValue] = useState(
    window.localStorage.getItem(key) ?? defaultValue
  );
  useEffect(() => {
    localStorage.setItem(key, value);
  }, [value]);
  return [value, setValue];
}

function useShortCut(keyCode = null, shiftKeyRequired = false) {
  const [shortcutFired, setFired] = useState(false);
  useEffect(() => {
    const listener = window.addEventListener("keydown", ({ key, shiftKey }) => {
      if (keyCode == key && shiftKey === shiftKeyRequired) {
        setFired(!shortcutFired);
      }
    });
    return () => {
      window.removeEventListener("keydown", listener);
    };
  }, [shortcutFired]);
  return shortcutFired;
}

function Login() {
  const [username, setUsername] = useStorage("", "username");
  return (
    <div className="App" style={{ margin: 24 }}>
      <input
        value={username}
        onChange={(event) => setUsername(event.target.value)}
      ></input>
      <p>{username}</p>
    </div>
  );
}

function Counter() {
  const [count, setCount] = useStorage(0, "count");
  return (
    <div className="App" style={{ margin: 24 }}>
      <h5>{count}</h5>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
    </div>
  );
}

function ShortCut() {
  const fired = useShortCut("S", true);
  return (
    <div>
      <div>
        <p>Short cut</p>
        <p>Shift + S = {fired ? "Pressed" : "Not pressed"}</p>
      </div>
    </div>
  );
}

function AnimationExample() {
  const { o, xyz, color } = useSpring({
    from: { o: 0, xyz: [0, 0, 0], color: "red" },
    o: 1,
    xyz: [10, 20, 5],
    color: "green",
  });
  return (
    <div>
      <animated.div
        style={{
          // If you can, use plain animated values like always, ...
          // You would do that in all cases where values "just fit"
          color,
          // Unless you need to interpolate them
          background: o.to((o) => `rgba(210, 57, 77, ${o})`),
          // Which works with arrays as well
          transform: xyz.to((x, y, z) => `translate3d(${x}px, ${y}px, ${z}px)`),
          // If you want to combine multiple values use the "interpolate" helper
          border: to([o, color], (o, c) => `${o * 10}px solid ${c}`),
          // You can also form ranges, even chain multiple interpolations
          padding: o
            .to({ range: [0, 0.5, 1], output: [0, 0, 10] })
            .to((o) => `${o}%`),
          // Interpolating strings (like up-front) through ranges is allowed ...
          borderColor: o.to({ range: [0, 1], output: ["red", "#ffaabb"] }),
          // There's also a shortcut for plain, optionless ranges ...
          opacity: o.to([0.1, 0.2, 0.6, 1], [1, 0.1, 0.5, 1]),
        }}
      >
        {o.to((n) => n.toFixed(2)) /* innerText interpolation ... */}
      </animated.div>
    </div>
  );
}
function App() {
  const showCounter = useShortCut("C", true);
  const showLogin = useShortCut("U", true);
  return (
    <div>
      {showLogin && <Login></Login>}
      {showCounter && <Counter></Counter>}
      <ShortCut></ShortCut>
      <AnimationExample></AnimationExample>
    </div>
  );
}

export default App;

 