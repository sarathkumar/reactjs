import { lazy, Suspense, SuspenseList } from "react";

const delay = (time) => {
  return (result) => {
    return new Promise((res) =>
      setTimeout(() => {
        res(result);
      }, time)
    );
  };
};

const Nav = lazy(() => import("./components/nav").then(delay(1300)));
const Footer = lazy(() => import("./components/footer").then(delay(3500)));
const Pricing = lazy(() => import("./components/pricing").then(delay(2000)));

function App() {
  return (
    <div className="container py-3">
        <Suspense fallback={<p>Loading...</p>}>
          <Nav></Nav>
        </Suspense>
        <Suspense fallback={< p>Loading...</p>}>
          <Pricing></Pricing>
        </Suspense>
        <Suspense fallback={<p>Loading...</p>}>
          <Footer></Footer>
        </Suspense>
    </div>
  );
}

export default App;
