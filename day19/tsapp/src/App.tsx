import logo from "./logo.svg";
import "./App.css";
import Message from "./components/message";
import Counter from "./components/counter";

// interface MessageProps {
//   userName: String;
// }

// interface MessageProps {
//   age: Number;
// }

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Message
          click={(event) => {
            console.log("Clicked", event);
          }}
          age={54}
          userName="Adith"
        >
          <p>Hello</p>
        </Message>

        <Counter></Counter>
      </header>
    </div>
  );
}

export default App;
