import { FC, useState, MouseEvent } from "react";

export type MessageProps = {
  userName: String;
  age: Number;
  click: (event: MouseEvent<HTMLButtonElement>) => void;
};

export type Todo = {
  todo: String;
  completed: boolean;
};
