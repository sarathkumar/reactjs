import { FC, useState, MouseEvent } from "react";
import {MessageProps} from './message.prop'


const Message: FC<MessageProps> = ({ age, userName, click }) => {
  const [show, setSetShow] = useState(true);
  return (
    <div>
      {show && (
        <p>
          Hello {userName} age: {age}
        </p>
      )}
      <button onClick={click}>Say hi from Button </button>
      <button
        onClick={(e) => {
          setSetShow(!show);
        }}
      >
        Show/Hide
      </button>
    </div>
  );
};
 

export default Message;
