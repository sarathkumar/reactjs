import { type } from "os";
import { useReducer } from "react";

type Action = {
  type: "increament" | "decrement";
  payload?: number | null;
};

function counterReducer(state: number, { type, payload }: Action) {
  if (type === "increament") {
    return state + (payload != null ? payload : 1);
  }
  if (type === "decrement") {
    return state - (payload != null ? payload : 1);
  }
  return 0;
}

export default function Counter() {
  const [count, dispatch] = useReducer(counterReducer, 0);
  return (
    <div>
      <p>Counter {count}</p>
      <button
        onClick={(event) => {
          dispatch({ type: "increament" });
        }}
      >
        +
      </button>
      <button
        onClick={(event) => {
          dispatch({ type: "decrement" });
        }}
      >
        -
      </button>
      <button
        onClick={(event) => {
          dispatch({ type: "increament",payload:5 });
        }}
      >
        + 5
      </button>
    </div>
  );
}
